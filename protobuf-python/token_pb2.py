# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: token.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.protobuf import timestamp_pb2 as google_dot_protobuf_dot_timestamp__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='token.proto',
  package='dumpexample',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=b'\n\x0btoken.proto\x12\x0b\x64umpexample\x1a\x1fgoogle/protobuf/timestamp.proto\"\xc3\x01\n\x03JWT\x12\r\n\x05token\x18\x01 \x01(\t\x12\x0b\n\x03iss\x18\x02 \x01(\t\x12\x0b\n\x03sub\x18\x03 \x01(\t\x12\x0b\n\x03\x61ud\x18\x04 \x01(\t\x12\'\n\x03\x65xp\x18\x05 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12\'\n\x03nbf\x18\x06 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12\'\n\x03iat\x18\x07 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12\x0b\n\x03jti\x18\x08 \x01(\tb\x06proto3'
  ,
  dependencies=[google_dot_protobuf_dot_timestamp__pb2.DESCRIPTOR,])




_JWT = _descriptor.Descriptor(
  name='JWT',
  full_name='dumpexample.JWT',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='token', full_name='dumpexample.JWT.token', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='iss', full_name='dumpexample.JWT.iss', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='sub', full_name='dumpexample.JWT.sub', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='aud', full_name='dumpexample.JWT.aud', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='exp', full_name='dumpexample.JWT.exp', index=4,
      number=5, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='nbf', full_name='dumpexample.JWT.nbf', index=5,
      number=6, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='iat', full_name='dumpexample.JWT.iat', index=6,
      number=7, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='jti', full_name='dumpexample.JWT.jti', index=7,
      number=8, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=62,
  serialized_end=257,
)

_JWT.fields_by_name['exp'].message_type = google_dot_protobuf_dot_timestamp__pb2._TIMESTAMP
_JWT.fields_by_name['nbf'].message_type = google_dot_protobuf_dot_timestamp__pb2._TIMESTAMP
_JWT.fields_by_name['iat'].message_type = google_dot_protobuf_dot_timestamp__pb2._TIMESTAMP
DESCRIPTOR.message_types_by_name['JWT'] = _JWT
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

JWT = _reflection.GeneratedProtocolMessageType('JWT', (_message.Message,), {
  'DESCRIPTOR' : _JWT,
  '__module__' : 'token_pb2'
  # @@protoc_insertion_point(class_scope:dumpexample.JWT)
  })
_sym_db.RegisterMessage(JWT)


# @@protoc_insertion_point(module_scope)
