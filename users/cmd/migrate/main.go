/*
   users service
   Copyright (C) 2020 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"log"
	"os"
	"time"

	"github.com/joho/godotenv"
	"gitlab.com/dogspeed/microservices-dumb-example/users/models"
	"gitlab.com/dogspeed/microservices-dumb-example/users/psql"
)

func main() {
	godotenv.Load()

	psqlClient := psql.NewPsqlClient(os.Getenv("DATABASE_HOST"),
		os.Getenv("DATABASE_PORT"), os.Getenv("DATABASE_NAME"),
		os.Getenv("DATABASE_USER"), os.Getenv("DATABASE_PASSWORD"))

	tries := 0
	is_open := false

	for tries < 5 && !is_open {
		if err := psqlClient.Open(true); err != nil {
			log.Printf("error openning database connection %s", err.Error())
			log.Printf("retry in 5 seconds")
			tries += 1
			time.Sleep(5 * time.Second)
		} else {
			is_open = true
		}
	}

	if !is_open {
		log.Printf("couldn't open database")
		os.Exit(1)
	}

	if err := psqlClient.Conn.AutoMigrate(&models.User{}); err != nil {
		log.Printf("error migrating users %s", err.Error())
		os.Exit(1)
	}

	if err := psqlClient.Conn.AutoMigrate(&models.Password{}); err != nil {
		log.Printf("error migrating passwords %s", err.Error())
		os.Exit(1)
	}

	if err := psqlClient.Conn.AutoMigrate(&models.Token{}); err != nil {
		log.Printf("error migrating tokens %s", err.Error())
		os.Exit(1)
	}

	log.Printf("database migrated")
}
