/*
   users service
   Copyright (C) 2020 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/dogspeed/microservices-dumb-example/users/app"
)

func main() {
	godotenv.Load()

	app, err := app.NewApp(os.Getenv("SERVICE_INTERFACE"),
		os.Getenv("SERVICE_PORT"), os.Getenv("TOKEN_SECRET"),
		os.Getenv("DATABASE_HOST"), os.Getenv("DATABASE_PORT"),
		os.Getenv("DATABASE_NAME"), os.Getenv("DATABASE_USER"),
		os.Getenv("DATABASE_PASSWORD"))
	if err != nil {
		log.Printf("error creating application %s", err.Error())
		os.Exit(1)
	}

	app.Run()
}
