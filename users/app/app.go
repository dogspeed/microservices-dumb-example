/*
   users service
   Copyright (C) 2020 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package app

import (
	"log"
	"time"

	"gitlab.com/dogspeed/microservices-dumb-example/users/psql"
	"gitlab.com/dogspeed/microservices-dumb-example/users/users"
)

/* Application structure */
type App struct {
	psqlClient  *psql.PsqlClient
	usersServer *users.UsersServer
}

/* Creates a new application */
func NewApp(serviceInterface, servicePort, tokenSecret, databaseHost,
	databasePort, databaseName, databaseUser, databasePassword string) (*App,
	error) {
	app := new(App)

	app.psqlClient = psql.NewPsqlClient(databaseHost, databasePort,
		databaseName, databaseUser, databasePassword)
	if err := app.psqlClient.Open(true); err != nil {
		log.Printf("error connecting to database %s", err.Error())
	}

	var err error

	app.usersServer, err = users.NewUsersServer(serviceInterface, servicePort,
		[]byte(tokenSecret), app.psqlClient)
	if err != nil {
		return nil, err
	}

	return app, nil
}

/* Runs application */
func (app *App) Run() {
	grpcStatus := make(chan struct{})

	go func() {
		defer close(grpcStatus)
		log.Print("started grpc server")
		app.usersServer.Serve()
	}()

	var end bool = false

	for !end {
		time.Sleep(100 * time.Millisecond)
		select {
		case <-grpcStatus:
			log.Print("grpc server has ended server is going to shutdown")
			end = true
		default:
		}
	}
}
