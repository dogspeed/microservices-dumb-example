module gitlab.com/dogspeed/microservices-dumb-example/users

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.1.2
	github.com/joho/godotenv v1.3.0
	gitlab.com/dogspeed/microservices-dumb-example/protobuf-go v0.0.0-20200913225800-967f4cccb9d0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7
	google.golang.org/grpc v1.32.0
	gorm.io/driver/postgres v1.0.0
	gorm.io/gorm v1.20.0
)
