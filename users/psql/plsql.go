/*
   users service
   Copyright (C) 2020 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package psql

import (
	"errors"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

/* Psql open connection try delay */
const psqlOpenDelay time.Duration = 10000000000 // nanoseconds = 10 seconds

/* Client structure */
type PsqlClient struct {
	Conn        *gorm.DB
	openLastTry time.Time
	host        string
	port        string
	name        string
	user        string
	password    string
}

/* Creates a new psql client */
func NewPsqlClient(host, port, name, user, password string) *PsqlClient {
	psqlClient := new(PsqlClient)

	psqlClient.openLastTry = time.Unix(0, 0)

	psqlClient.host = host
	psqlClient.port = port
	psqlClient.name = name
	psqlClient.user = user
	psqlClient.password = password

	return psqlClient
}

/* Opens the database connection */
func (psqlClient *PsqlClient) Open(force bool) error {
	if !force {
		if time.Now().Sub(psqlClient.openLastTry) <= psqlOpenDelay {
			return errors.New("not enough time since last try to open database")
		}
	}

	psqlClient.openLastTry = time.Now()

	var err error

	/* TODO: implement SSL */
	dsn := "host=" + psqlClient.host +
		" port=" + psqlClient.port +
		" user=" + psqlClient.user +
		" dbname=" + psqlClient.name +
		" sslmode=disable" +
		" password=" + psqlClient.password
	psqlClient.Conn, err = gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Silent),
	})

	if err != nil {
		psqlClient.Conn = nil
		return err
	}

	return nil
}
