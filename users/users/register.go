/*
   users service
   Copyright (C) 2020 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package users

import (
	"log"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/google/uuid"
	pb "gitlab.com/dogspeed/microservices-dumb-example/protobuf-go"
	"gitlab.com/dogspeed/microservices-dumb-example/users/models"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

/* Register function handler */
func (usersServer *UsersServer) Register(_ context.Context,
	in *pb.User) (*pb.UserJWT, error) {
	if in.GetEmail() == "" {
		return nil, status.Errorf(codes.InvalidArgument,
			"missing required argument email")
	}

	if in.GetUsername() == "" {
		return nil, status.Errorf(codes.InvalidArgument,
			"missing required argument username")
	}

	if in.GetPassword() == "" {
		return nil, status.Errorf(codes.InvalidArgument,
			"missing required argument password")
	}

	if user, _ := models.GetUserEmail(usersServer.psqlClient,
		in.GetEmail()); user != nil {
		return nil, status.Errorf(codes.AlreadyExists, "email already in use")
	}

	if user, _ := models.GetUserUsername(usersServer.psqlClient,
		in.GetUsername()); user != nil {
		return nil, status.Errorf(codes.AlreadyExists,
			"username already in use")
	}

	user := new(models.User)
	user.UserUUID = uuid.New()
	user.Email = in.GetEmail()
	user.Username = in.GetUsername()
	user.CreatedAt = time.Now()

	password := new(models.Password)
	password.ChangedAt = time.Now()
	password.User = user
	password.Password = in.GetPassword()

	nowTime := time.Now()

	expiresAtRefresh := nowTime.Add(tokenRefresh * time.Millisecond)
	token := new(models.Token)
	tokenString, err := models.NewToken(tokenAudience, expiresAtRefresh.Unix(),
		"", nowTime.Unix(), tokenIssuer, nowTime.Unix(), user.UserUUID.String(),
		usersServer.tokenSecret)
	if err != nil {
		log.Printf("error creating refresh token %s", err.Error())
		return nil, status.Errorf(codes.Internal, "error creating user")
	}
	token.CreatedAt = nowTime
	token.ExpiresAt = expiresAtRefresh
	token.User = user
	token.Token = tokenString

	if err := user.Add(usersServer.psqlClient); err != nil {
		log.Printf("error adding user %s", err.Error())
		return nil, status.Errorf(codes.Internal, "error creating user")
	}

	if err := password.Add(usersServer.psqlClient); err != nil {
		log.Printf("error adding password %s", err.Error())
		return nil, status.Errorf(codes.Internal, "error creating user")
	}

	if err := token.Add(usersServer.psqlClient); err != nil {
		log.Printf("error adding token %s", err.Error())
		return nil, status.Errorf(codes.Internal, "error creating user")
	}

	/* Create access token */
	expiresAtAccess := nowTime.Add(tokenAccess * time.Millisecond)
	accessString, err := models.NewToken(tokenAudience, expiresAtAccess.Unix(),
		"", nowTime.Unix(), tokenIssuer, nowTime.Unix(), user.UserUUID.String(),
		usersServer.tokenSecret)
	if err != nil {
		log.Printf("error creating access token %s", err.Error())
	}

	log.Printf("created new user %s %s %s", user.UserUUID.String(), user.Email,
		user.Username)

	createdAt, _ := ptypes.TimestampProto(user.CreatedAt)
	changedAt, _ := ptypes.TimestampProto(password.ChangedAt)
	expRefresh, _ := ptypes.TimestampProto(expiresAtRefresh)
	expAccess, _ := ptypes.TimestampProto(expiresAtAccess)
	nowTimeProto, _ := ptypes.TimestampProto(nowTime)

	return &pb.UserJWT{
		User: &pb.User{
			UserUuid:  user.UserUUID.String(),
			Email:     user.Email,
			Username:  user.Username,
			CreatedAt: createdAt,
			Password:  "",
			ChangedAt: changedAt,
		},
		Refresh: &pb.JWT{
			Token: tokenString,
			Iss:   tokenIssuer,
			Sub:   user.UserUUID.String(),
			Aud:   tokenAudience,
			Exp:   expRefresh,
			Nbf:   nowTimeProto,
			Iat:   nowTimeProto,
			Jti:   "",
		},
		Access: &pb.JWT{
			Token: accessString,
			Iss:   tokenIssuer,
			Sub:   user.UserUUID.String(),
			Aud:   tokenAudience,
			Exp:   expAccess,
			Nbf:   nowTimeProto,
			Iat:   nowTimeProto,
			Jti:   "",
		},
	}, nil
}
