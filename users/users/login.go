/*
   users service
   Copyright (C) 2020 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package users

import (
	"log"
	"time"

	"github.com/golang/protobuf/ptypes"
	pb "gitlab.com/dogspeed/microservices-dumb-example/protobuf-go"
	"gitlab.com/dogspeed/microservices-dumb-example/users/models"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

/* Login function handler */
func (usersServer *UsersServer) Login(_ context.Context,
	in *pb.User) (*pb.UserJWT, error) {
	if in.GetEmail() == "" && in.GetUsername() == "" {
		return nil, status.Errorf(codes.InvalidArgument,
			"missing required argument email or username")
	}

	if in.GetPassword() == "" {
		return nil, status.Errorf(codes.InvalidArgument,
			"missing required argument password")
	}

	var user *models.User
	var err error

	if in.GetEmail() != "" {
		user, err = models.GetUserEmail(usersServer.psqlClient, in.GetEmail())
	} else {
		user, err = models.GetUserUsername(usersServer.psqlClient,
			in.GetUsername())
	}
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "bad credentials")
	}

	password, err := models.GetPasswordOwner(usersServer.psqlClient, user.ID)
	if err != nil {
		log.Printf("error obtaining user password %s", err.Error())
		return nil, status.Errorf(codes.Internal, "canno't login user")
	}

	if err = password.Compare(in.GetPassword()); err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "bad credentials")
	}

	nowTime := time.Now()

	expiresAtRefresh := nowTime.Add(tokenRefresh * time.Millisecond)
	token := new(models.Token)
	tokenString, err := models.NewToken(tokenAudience, expiresAtRefresh.Unix(),
		"", nowTime.Unix(), tokenIssuer, nowTime.Unix(), user.UserUUID.String(),
		usersServer.tokenSecret)
	if err != nil {
		log.Printf("error creating refresh token %s", err.Error())
		return nil, status.Errorf(codes.Internal, "error creating user")
	}
	token.CreatedAt = nowTime
	token.ExpiresAt = expiresAtRefresh
	token.User = user
	token.Token = tokenString

	if err := token.Add(usersServer.psqlClient); err != nil {
		log.Printf("error adding token %s", err.Error())
		return nil, status.Errorf(codes.Internal, "error creating user")
	}

	/* Create access token */
	expiresAtAccess := nowTime.Add(tokenAccess * time.Millisecond)
	accessString, err := models.NewToken(tokenAudience, expiresAtAccess.Unix(),
		"", nowTime.Unix(), tokenIssuer, nowTime.Unix(), user.UserUUID.String(),
		usersServer.tokenSecret)
	if err != nil {
		log.Printf("error creating access token %s", err.Error())
	}

	createdAt, _ := ptypes.TimestampProto(user.CreatedAt)
	changedAt, _ := ptypes.TimestampProto(password.ChangedAt)
	expRefresh, _ := ptypes.TimestampProto(expiresAtRefresh)
	expAccess, _ := ptypes.TimestampProto(expiresAtAccess)
	nowTimeProto, _ := ptypes.TimestampProto(nowTime)

	return &pb.UserJWT{
		User: &pb.User{
			UserUuid:  user.UserUUID.String(),
			Email:     user.Email,
			Username:  user.Username,
			CreatedAt: createdAt,
			Password:  "",
			ChangedAt: changedAt,
		},
		Refresh: &pb.JWT{
			Token: tokenString,
			Iss:   tokenIssuer,
			Sub:   user.UserUUID.String(),
			Aud:   tokenAudience,
			Exp:   expRefresh,
			Nbf:   nowTimeProto,
			Iat:   nowTimeProto,
			Jti:   "",
		},
		Access: &pb.JWT{
			Token: accessString,
			Iss:   tokenIssuer,
			Sub:   user.UserUUID.String(),
			Aud:   tokenAudience,
			Exp:   expAccess,
			Nbf:   nowTimeProto,
			Iat:   nowTimeProto,
			Jti:   "",
		},
	}, nil
}
