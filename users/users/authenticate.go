/*
   users service
   Copyright (C) 2020 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package users

import (
	_ "log"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/golang/protobuf/ptypes"
	"github.com/google/uuid"
	pb "gitlab.com/dogspeed/microservices-dumb-example/protobuf-go"
	"gitlab.com/dogspeed/microservices-dumb-example/users/models"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

/* Login function handler */
func (usersServer *UsersServer) Authenticate(_ context.Context,
	in *pb.JWT) (*pb.User, error) {
	if in.GetToken() == "" {
		return nil, status.Errorf(codes.InvalidArgument,
			"missing required argument token")
	}

	token, err := models.ValidateToken(in.GetToken(), usersServer.tokenSecret)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "invalid token %s",
			err.Error())
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, status.Errorf(codes.Unauthenticated,
			"invalid token bad claims")
	}

	sub, ok := claims["sub"]
	if !ok {
		return nil, status.Errorf(codes.Unauthenticated,
			"invalid token missing subject")
	}

	userUuid, err := uuid.Parse(sub.(string))
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated,
			"invalid token bad subject")
	}

	user, err := models.GetUserUserUUID(usersServer.psqlClient, userUuid)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated,
			"invalid token unknown user")
	}

	/* TODO: if refresh token check database */

	createdAt, _ := ptypes.TimestampProto(user.CreatedAt)

	return &pb.User{
		UserUuid:  user.UserUUID.String(),
		Email:     user.Email,
		Username:  user.Username,
		CreatedAt: createdAt,
		Password:  "",
		ChangedAt: nil,
	}, nil
}
