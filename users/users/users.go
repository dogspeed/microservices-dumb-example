/*
   users service
   Copyright (C) 2020 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package users

import (
	"log"
	"net"

	pb "gitlab.com/dogspeed/microservices-dumb-example/protobuf-go"
	"gitlab.com/dogspeed/microservices-dumb-example/users/psql"
	"google.golang.org/grpc"
)

/* Token audience */
const tokenAudience = "localhost"

/* Token issuer */
const tokenIssuer = "microservices-dumb-example"

/* Token refresh duration */
const tokenRefresh = 2592000000 /* milliseconds = 30 days */

/* Token access duration */
const tokenAccess = 1800000 /* milliseconds = 30 minutes */

/* Structure to define the users server */
/* Psql client must be initialized */
type UsersServer struct {
	server      *grpc.Server
	ln          net.Listener
	psqlClient  *psql.PsqlClient
	tokenSecret []byte
	pb.UnimplementedUsersServer
}

/* Creates a new users server */
func NewUsersServer(ifc, port string, tokenSecret []byte,
	psqlClient *psql.PsqlClient) (*UsersServer, error) {
	usersServer := new(UsersServer)

	var err error

	usersServer.ln, err = net.Listen("tcp", ifc+":"+port)
	if err != nil {
		return nil, err
	}

	usersServer.tokenSecret = tokenSecret

	usersServer.psqlClient = psqlClient

	var opts []grpc.ServerOption
	/* TODO: TLS */
	usersServer.server = grpc.NewServer(opts...)

	pb.RegisterUsersServer(usersServer.server, usersServer)

	return usersServer, nil
}

/* Serves users server */
func (usersServer *UsersServer) Serve() {
	usersServer.server.Serve(usersServer.ln)
}

/* Stops user server */
func (usersServer *UsersServer) Stop() {
	log.Print("gracefully shutting down users server")
	usersServer.server.GracefulStop()
}
