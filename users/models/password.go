/*
   users service
   Copyright (C) 2020 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package models

import (
	"time"

	"gitlab.com/dogspeed/microservices-dumb-example/users/psql"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

/* Structure to define a password */
type Password struct {
	Owner        int       `gorm:"column:owner;type:bigint;primary key"`
	PasswordHash string    `gorm:"column:password_hash;type:text;not null"`
	ChangedAt    time.Time `gorm:"column:changed_at;type:timestamp with time zone;not null;default:(now() at time zone 'utc')"`
	User         *User     `gorm:"foreignKey:Owner;references:id;constraint:OnUpdate:RESTRICT,OnDelete:RESTRICT"`
	Password     string
}

/* Set table name */
func (Password) TableName() string {
	return "passwords"
}

/* Returns a database filter by owner */
func filterPasswordOwner(conn *gorm.DB, owner int) *gorm.DB {
	return conn.Where("owner = ?", owner)
}

/* Retrieves a password from a database connection */
func getPassword(filter *gorm.DB) (*Password, error) {
	password := new(Password)

	if err := filter.First(&password).Error; err != nil {
		return nil, err
	}

	return password, nil
}

/* Retrieves a password given the owner */
func GetPasswordOwner(psqlClient *psql.PsqlClient, owner int) (*Password,
	error) {
	if psqlClient.Conn == nil {
		if err := psqlClient.Open(false); err != nil {
			return nil, err
		}
	}

	return getPassword(filterPasswordOwner(psqlClient.Conn, owner))
}

/* Creates a new password on database */
func (password *Password) add(conn *gorm.DB) error {
	result := conn.Create(password)

	return result.Error
}

/* Creates a new password */
func (password *Password) Add(psqlClient *psql.PsqlClient) error {
	if psqlClient.Conn == nil {
		if err := psqlClient.Open(false); err != nil {
			return err
		}
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(password.Password),
		bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	password.PasswordHash = string(hash)

	return password.add(psqlClient.Conn)
}

/* Compares password */
func (password *Password) Compare(inputPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(password.PasswordHash),
		[]byte(inputPassword))
}
