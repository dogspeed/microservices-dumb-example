/*
   users service
   Copyright (C) 2020 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package models

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/dogspeed/microservices-dumb-example/users/psql"
	"gorm.io/gorm"
)

/* Structure to define a user */
type User struct {
	ID        int       `gorm:"column:id;type:bigserial;auto_increment;primary key"`
	UserUUID  uuid.UUID `gorm:"column:user_uuid;type:uuid;unique;not null"`
	Email     string    `gorm:"column:email;type:text;unique;not null"`
	Username  string    `gorm:"column:username;type:varchar(18);unique;not null"`
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp with time zone;not null;default:(now() at time zone 'utc')"`
}

/* Set table name */
func (User) TableName() string {
	return "users"
}

/* Returns a database filter of users by ID */
func filterUserID(conn *gorm.DB, id int) *gorm.DB {
	return conn.Where("id = ?", id)
}

/* Returns a database filter of users by user UUID */
func filterUserUserUUID(conn *gorm.DB, userUUID uuid.UUID) *gorm.DB {
	return conn.Where("user_uuid = ?", userUUID.String())
}

/* Returns a database filter of users by email */
func filterUserEmail(conn *gorm.DB, email string) *gorm.DB {
	return conn.Where("email = ?", email)
}

/* Returns a database filter of users by username */
func filterUserUsername(conn *gorm.DB, username string) *gorm.DB {
	return conn.Where("username = ?", username)
}

/* Retrieves a user from a database connection */
func getUser(filter *gorm.DB) (*User, error) {
	user := new(User)

	if err := filter.First(&user).Error; err != nil {
		return nil, err
	}

	return user, nil
}

/* Retrieves a user given the ID */
func GetUserID(psqlClient *psql.PsqlClient, id int) (*User, error) {
	if psqlClient.Conn == nil {
		if err := psqlClient.Open(false); err != nil {
			return nil, err
		}
	}

	return getUser(filterUserID(psqlClient.Conn, id))
}

/* Retrieves a user given the user UUID */
func GetUserUserUUID(psqlClient *psql.PsqlClient, userUUID uuid.UUID) (*User,
	error) {
	if psqlClient.Conn == nil {
		if err := psqlClient.Open(false); err != nil {
			return nil, err
		}
	}

	return getUser(filterUserUserUUID(psqlClient.Conn, userUUID))
}

/* Retrieves a user given the email */
func GetUserEmail(psqlClient *psql.PsqlClient, email string) (*User, error) {
	if psqlClient.Conn == nil {
		if err := psqlClient.Open(false); err != nil {
			return nil, err
		}
	}

	return getUser(filterUserEmail(psqlClient.Conn, email))
}

/* Retrieves a user given the username */
func GetUserUsername(psqlClient *psql.PsqlClient, username string) (*User,
	error) {
	if psqlClient.Conn == nil {
		if err := psqlClient.Open(false); err != nil {
			return nil, err
		}
	}

	return getUser(filterUserUsername(psqlClient.Conn, username))
}

/* Creates a new user on database */
func (user *User) add(conn *gorm.DB) error {
	result := conn.Create(user)

	return result.Error
}

/* Creates a new user */
func (user *User) Add(psqlClient *psql.PsqlClient) error {
	if psqlClient.Conn == nil {
		if err := psqlClient.Open(false); err != nil {
			return err
		}
	}

	return user.add(psqlClient.Conn)
}
