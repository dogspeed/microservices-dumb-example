/*
   users service
   Copyright (C) 2020 Bruno Mondelo Giaramita

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package models

import (
	"errors"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/dogspeed/microservices-dumb-example/users/psql"
	"gorm.io/gorm"
)

/* Structure to define a token */
type Token struct {
	ID        int       `gorm:"column:id;type:bigserial;auto_increment;primary key"`
	Owner     int       `gorm:"column:owner;type:bigint;not null"`
	TokenHash string    `gorm:"column:token_hash;type:text;not null"`
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp with time zone;not null"`
	ExpiresAt time.Time `gorm:"column:expires_at;type:timestamp with time zone;not null"`
	User      *User     `gorm:"foreignKey:Owner;references:id;constraint:OnUpdate:RESTRICT,OnDelete:RESTRICT"`
	Token     string
}

/* Set table name */
func (Token) TableName() string {
	return "tokens"
}

/* Creates a new JWT */
func NewToken(audience string, expiresAt int64, id string, issuedAt int64,
	issuer string, notBefore int64, subject string, secret []byte) (string,
	error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &jwt.StandardClaims{
		Audience:  audience,
		ExpiresAt: expiresAt,
		Id:        id,
		IssuedAt:  issuedAt,
		Issuer:    issuer,
		NotBefore: notBefore,
		Subject:   subject,
	})

	signed, err := token.SignedString(secret)
	if err != nil {
		return "", err
	}

	return signed, nil
}

/* Obtains hash from token */
func HashToken(token string) (string, error) {
	parts := strings.Split(token, ".")

	if len(parts) != 3 {
		return "", errors.New("bad token format")
	}

	return parts[2], nil
}

/* Validates token */
func ValidateToken(token string, secret []byte) (*jwt.Token, error) {
	tokenParsed, err := jwt.Parse(token, func(
		token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("unexpected signing method")
		}
		return secret, nil
	})
	if err != nil {
		return nil, err
	}

	if !tokenParsed.Valid {
		return nil, errors.New("invalid token")
	}

	return tokenParsed, nil
}

/* Returns a database filter by owner and token hash */
func filterTokenOwnerTokenHash(conn *gorm.DB, owner int,
	tokenHash string) *gorm.DB {
	return conn.Where("owner = ? AND token_hash = ?", owner, tokenHash)
}

/* Retrieves a token from a database connection */
func getToken(filter *gorm.DB) (*Token, error) {
	token := new(Token)

	if err := filter.First(&token).Error; err != nil {
		return nil, err
	}

	return token, nil
}

/* Retrieves a token given the password and token hash */
func GetTokenOwnerTokenHash(psqlClient *psql.PsqlClient, owner int,
	tokenHash string) (*Token, error) {
	if psqlClient.Conn == nil {
		if err := psqlClient.Open(false); err != nil {
			return nil, err
		}
	}

	return getToken(filterTokenOwnerTokenHash(psqlClient.Conn, owner,
		tokenHash))
}

/* Creates a new token on database */
func (token *Token) add(conn *gorm.DB) error {
	result := conn.Create(token)

	return result.Error
}

/* Creates a new token */
func (token *Token) Add(psqlClient *psql.PsqlClient) error {
	if psqlClient.Conn == nil {
		if err := psqlClient.Open(false); err != nil {
			return err
		}
	}

	hash, err := HashToken(token.Token)
	if err != nil {
		return err
	}

	token.TokenHash = hash

	return token.add(psqlClient.Conn)
}

/* Deletes a token on database */
func (token *Token) delete(conn *gorm.DB) error {
	result := conn.Delete(token)

	return result.Error
}

/* Deletes a token */
func (token *Token) Delete(psqlClient *psql.PsqlClient) error {
	if psqlClient.Conn == nil {
		if err := psqlClient.Open(false); err != nil {
			return err
		}
	}

	return token.delete(psqlClient.Conn)
}
