// Code generated by protoc-gen-go. DO NOT EDIT.
// source: users.proto

package dumpexample

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	empty "github.com/golang/protobuf/ptypes/empty"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// User message type
type User struct {
	UserUuid             string               `protobuf:"bytes,1,opt,name=user_uuid,json=userUuid,proto3" json:"user_uuid,omitempty"`
	Email                string               `protobuf:"bytes,2,opt,name=email,proto3" json:"email,omitempty"`
	Username             string               `protobuf:"bytes,3,opt,name=username,proto3" json:"username,omitempty"`
	CreatedAt            *timestamp.Timestamp `protobuf:"bytes,4,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	Password             string               `protobuf:"bytes,5,opt,name=password,proto3" json:"password,omitempty"`
	ChangedAt            *timestamp.Timestamp `protobuf:"bytes,6,opt,name=changed_at,json=changedAt,proto3" json:"changed_at,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *User) Reset()         { *m = User{} }
func (m *User) String() string { return proto.CompactTextString(m) }
func (*User) ProtoMessage()    {}
func (*User) Descriptor() ([]byte, []int) {
	return fileDescriptor_030765f334c86cea, []int{0}
}

func (m *User) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_User.Unmarshal(m, b)
}
func (m *User) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_User.Marshal(b, m, deterministic)
}
func (m *User) XXX_Merge(src proto.Message) {
	xxx_messageInfo_User.Merge(m, src)
}
func (m *User) XXX_Size() int {
	return xxx_messageInfo_User.Size(m)
}
func (m *User) XXX_DiscardUnknown() {
	xxx_messageInfo_User.DiscardUnknown(m)
}

var xxx_messageInfo_User proto.InternalMessageInfo

func (m *User) GetUserUuid() string {
	if m != nil {
		return m.UserUuid
	}
	return ""
}

func (m *User) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *User) GetUsername() string {
	if m != nil {
		return m.Username
	}
	return ""
}

func (m *User) GetCreatedAt() *timestamp.Timestamp {
	if m != nil {
		return m.CreatedAt
	}
	return nil
}

func (m *User) GetPassword() string {
	if m != nil {
		return m.Password
	}
	return ""
}

func (m *User) GetChangedAt() *timestamp.Timestamp {
	if m != nil {
		return m.ChangedAt
	}
	return nil
}

// User JWT message type
type UserJWT struct {
	User                 *User    `protobuf:"bytes,1,opt,name=user,proto3" json:"user,omitempty"`
	Refresh              *JWT     `protobuf:"bytes,2,opt,name=refresh,proto3" json:"refresh,omitempty"`
	Access               *JWT     `protobuf:"bytes,3,opt,name=access,proto3" json:"access,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserJWT) Reset()         { *m = UserJWT{} }
func (m *UserJWT) String() string { return proto.CompactTextString(m) }
func (*UserJWT) ProtoMessage()    {}
func (*UserJWT) Descriptor() ([]byte, []int) {
	return fileDescriptor_030765f334c86cea, []int{1}
}

func (m *UserJWT) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserJWT.Unmarshal(m, b)
}
func (m *UserJWT) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserJWT.Marshal(b, m, deterministic)
}
func (m *UserJWT) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserJWT.Merge(m, src)
}
func (m *UserJWT) XXX_Size() int {
	return xxx_messageInfo_UserJWT.Size(m)
}
func (m *UserJWT) XXX_DiscardUnknown() {
	xxx_messageInfo_UserJWT.DiscardUnknown(m)
}

var xxx_messageInfo_UserJWT proto.InternalMessageInfo

func (m *UserJWT) GetUser() *User {
	if m != nil {
		return m.User
	}
	return nil
}

func (m *UserJWT) GetRefresh() *JWT {
	if m != nil {
		return m.Refresh
	}
	return nil
}

func (m *UserJWT) GetAccess() *JWT {
	if m != nil {
		return m.Access
	}
	return nil
}

func init() {
	proto.RegisterType((*User)(nil), "dumpexample.User")
	proto.RegisterType((*UserJWT)(nil), "dumpexample.UserJWT")
}

func init() { proto.RegisterFile("users.proto", fileDescriptor_030765f334c86cea) }

var fileDescriptor_030765f334c86cea = []byte{
	// 391 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x94, 0x92, 0x4d, 0xeb, 0xda, 0x40,
	0x10, 0xc6, 0x93, 0x7f, 0x4d, 0xd4, 0x49, 0x0f, 0xed, 0x22, 0x25, 0xc4, 0x43, 0x25, 0x50, 0x90,
	0x42, 0x23, 0xa4, 0xf5, 0xd0, 0xa3, 0x87, 0x5e, 0xc4, 0x53, 0x88, 0x78, 0x94, 0x35, 0x19, 0x63,
	0xa8, 0xc9, 0x86, 0x7d, 0xc1, 0xf6, 0x0b, 0xf4, 0x6b, 0xf5, 0xe3, 0xf4, 0x6b, 0x94, 0xdd, 0x24,
	0xf4, 0x25, 0x1e, 0xfc, 0x1f, 0x67, 0x9e, 0xdf, 0x3c, 0xfb, 0xcc, 0xb0, 0xe0, 0x29, 0x81, 0x5c,
	0x44, 0x0d, 0x67, 0x92, 0x11, 0x2f, 0x57, 0x55, 0x83, 0xdf, 0x68, 0xd5, 0x5c, 0x31, 0x98, 0x17,
	0x8c, 0x15, 0x57, 0x5c, 0x19, 0xe9, 0xa4, 0xce, 0x2b, 0xac, 0x1a, 0xf9, 0xbd, 0x25, 0x83, 0xb7,
	0xff, 0x8b, 0xb2, 0xac, 0x50, 0x48, 0x5a, 0x35, 0x1d, 0xe0, 0x49, 0xf6, 0x15, 0xeb, 0xb6, 0x08,
	0x7f, 0xd9, 0x30, 0xda, 0x0b, 0xe4, 0x64, 0x0e, 0x53, 0xfd, 0xde, 0x51, 0xa9, 0x32, 0xf7, 0xed,
	0x85, 0xbd, 0x9c, 0x26, 0x13, 0xdd, 0xd8, 0xab, 0x32, 0x27, 0x33, 0x70, 0xb0, 0xa2, 0xe5, 0xd5,
	0x7f, 0x32, 0x42, 0x5b, 0x90, 0x00, 0x0c, 0x51, 0xd3, 0x0a, 0xfd, 0x17, 0x7f, 0x26, 0x74, 0x4d,
	0x3e, 0x03, 0x64, 0x1c, 0xa9, 0xc4, 0xfc, 0x48, 0xa5, 0x3f, 0x5a, 0xd8, 0x4b, 0x2f, 0x0e, 0xa2,
	0x36, 0x5a, 0xd4, 0x47, 0x8b, 0xd2, 0x3e, 0x5a, 0x32, 0xed, 0xe8, 0x8d, 0xd4, 0xb6, 0x0d, 0x15,
	0xe2, 0xc6, 0x78, 0xee, 0x3b, 0xad, 0x6d, 0x5f, 0x1b, 0xdb, 0x0b, 0xad, 0x8b, 0xd6, 0xd6, 0x7d,
	0xc0, 0xb6, 0xa5, 0x37, 0x32, 0xfc, 0x61, 0xc3, 0x58, 0x6f, 0xba, 0x3d, 0xa4, 0xe4, 0x1d, 0x8c,
	0x74, 0x52, 0xb3, 0xa7, 0x17, 0xbf, 0x8e, 0xfe, 0x3a, 0x6e, 0xa4, 0x99, 0xc4, 0xc8, 0xe4, 0x3d,
	0x8c, 0x39, 0x9e, 0x39, 0x8a, 0x8b, 0x59, 0xdc, 0x8b, 0x5f, 0xfd, 0x43, 0x6e, 0x0f, 0x69, 0xd2,
	0x03, 0x64, 0x09, 0x2e, 0xcd, 0x32, 0x14, 0xc2, 0x9c, 0xe2, 0x1e, 0xda, 0xe9, 0xf1, 0xcf, 0x27,
	0x70, 0xf4, 0x23, 0x82, 0xac, 0x61, 0x92, 0x60, 0x51, 0x0a, 0x89, 0x9c, 0x0c, 0x43, 0x04, 0xb3,
	0x41, 0x6b, 0x7b, 0x48, 0x43, 0x8b, 0xc4, 0xe0, 0xec, 0x58, 0x51, 0xd6, 0xcf, 0x99, 0xf9, 0x04,
	0xee, 0x8e, 0x15, 0x4c, 0x49, 0x32, 0x08, 0x16, 0xbc, 0x19, 0x1c, 0xf0, 0x8b, 0xfe, 0x4f, 0xa1,
	0x45, 0xd6, 0xf0, 0x72, 0xa3, 0xe4, 0x05, 0x6b, 0x59, 0x66, 0x54, 0xe2, 0x9d, 0xd9, 0x61, 0x84,
	0xd0, 0x22, 0x2b, 0x18, 0x27, 0xdd, 0x59, 0x86, 0x13, 0x83, 0x4e, 0x68, 0x91, 0x0f, 0xe0, 0x24,
	0x58, 0xe3, 0xed, 0x31, 0xfc, 0xe4, 0x9a, 0xa0, 0x1f, 0x7f, 0x07, 0x00, 0x00, 0xff, 0xff, 0x13,
	0x4b, 0xbf, 0x32, 0x22, 0x03, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// UsersClient is the client API for Users service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type UsersClient interface {
	// Creates a new user
	Register(ctx context.Context, in *User, opts ...grpc.CallOption) (*UserJWT, error)
	// Logins a user
	Login(ctx context.Context, in *User, opts ...grpc.CallOption) (*UserJWT, error)
	// Logouts a user
	Logout(ctx context.Context, in *JWT, opts ...grpc.CallOption) (*empty.Empty, error)
	// Authenticates a user given an access token
	Authenticate(ctx context.Context, in *JWT, opts ...grpc.CallOption) (*User, error)
	// Refresh an access token
	Refresh(ctx context.Context, in *JWT, opts ...grpc.CallOption) (*JWT, error)
	// Renews a refresh token
	Renew(ctx context.Context, in *JWT, opts ...grpc.CallOption) (*JWT, error)
}

type usersClient struct {
	cc *grpc.ClientConn
}

func NewUsersClient(cc *grpc.ClientConn) UsersClient {
	return &usersClient{cc}
}

func (c *usersClient) Register(ctx context.Context, in *User, opts ...grpc.CallOption) (*UserJWT, error) {
	out := new(UserJWT)
	err := c.cc.Invoke(ctx, "/dumpexample.Users/Register", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *usersClient) Login(ctx context.Context, in *User, opts ...grpc.CallOption) (*UserJWT, error) {
	out := new(UserJWT)
	err := c.cc.Invoke(ctx, "/dumpexample.Users/Login", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *usersClient) Logout(ctx context.Context, in *JWT, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, "/dumpexample.Users/Logout", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *usersClient) Authenticate(ctx context.Context, in *JWT, opts ...grpc.CallOption) (*User, error) {
	out := new(User)
	err := c.cc.Invoke(ctx, "/dumpexample.Users/Authenticate", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *usersClient) Refresh(ctx context.Context, in *JWT, opts ...grpc.CallOption) (*JWT, error) {
	out := new(JWT)
	err := c.cc.Invoke(ctx, "/dumpexample.Users/Refresh", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *usersClient) Renew(ctx context.Context, in *JWT, opts ...grpc.CallOption) (*JWT, error) {
	out := new(JWT)
	err := c.cc.Invoke(ctx, "/dumpexample.Users/Renew", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// UsersServer is the server API for Users service.
type UsersServer interface {
	// Creates a new user
	Register(context.Context, *User) (*UserJWT, error)
	// Logins a user
	Login(context.Context, *User) (*UserJWT, error)
	// Logouts a user
	Logout(context.Context, *JWT) (*empty.Empty, error)
	// Authenticates a user given an access token
	Authenticate(context.Context, *JWT) (*User, error)
	// Refresh an access token
	Refresh(context.Context, *JWT) (*JWT, error)
	// Renews a refresh token
	Renew(context.Context, *JWT) (*JWT, error)
}

// UnimplementedUsersServer can be embedded to have forward compatible implementations.
type UnimplementedUsersServer struct {
}

func (*UnimplementedUsersServer) Register(ctx context.Context, req *User) (*UserJWT, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Register not implemented")
}
func (*UnimplementedUsersServer) Login(ctx context.Context, req *User) (*UserJWT, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Login not implemented")
}
func (*UnimplementedUsersServer) Logout(ctx context.Context, req *JWT) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Logout not implemented")
}
func (*UnimplementedUsersServer) Authenticate(ctx context.Context, req *JWT) (*User, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Authenticate not implemented")
}
func (*UnimplementedUsersServer) Refresh(ctx context.Context, req *JWT) (*JWT, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Refresh not implemented")
}
func (*UnimplementedUsersServer) Renew(ctx context.Context, req *JWT) (*JWT, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Renew not implemented")
}

func RegisterUsersServer(s *grpc.Server, srv UsersServer) {
	s.RegisterService(&_Users_serviceDesc, srv)
}

func _Users_Register_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(User)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UsersServer).Register(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/dumpexample.Users/Register",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UsersServer).Register(ctx, req.(*User))
	}
	return interceptor(ctx, in, info, handler)
}

func _Users_Login_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(User)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UsersServer).Login(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/dumpexample.Users/Login",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UsersServer).Login(ctx, req.(*User))
	}
	return interceptor(ctx, in, info, handler)
}

func _Users_Logout_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(JWT)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UsersServer).Logout(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/dumpexample.Users/Logout",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UsersServer).Logout(ctx, req.(*JWT))
	}
	return interceptor(ctx, in, info, handler)
}

func _Users_Authenticate_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(JWT)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UsersServer).Authenticate(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/dumpexample.Users/Authenticate",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UsersServer).Authenticate(ctx, req.(*JWT))
	}
	return interceptor(ctx, in, info, handler)
}

func _Users_Refresh_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(JWT)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UsersServer).Refresh(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/dumpexample.Users/Refresh",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UsersServer).Refresh(ctx, req.(*JWT))
	}
	return interceptor(ctx, in, info, handler)
}

func _Users_Renew_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(JWT)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UsersServer).Renew(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/dumpexample.Users/Renew",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UsersServer).Renew(ctx, req.(*JWT))
	}
	return interceptor(ctx, in, info, handler)
}

var _Users_serviceDesc = grpc.ServiceDesc{
	ServiceName: "dumpexample.Users",
	HandlerType: (*UsersServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Register",
			Handler:    _Users_Register_Handler,
		},
		{
			MethodName: "Login",
			Handler:    _Users_Login_Handler,
		},
		{
			MethodName: "Logout",
			Handler:    _Users_Logout_Handler,
		},
		{
			MethodName: "Authenticate",
			Handler:    _Users_Authenticate_Handler,
		},
		{
			MethodName: "Refresh",
			Handler:    _Users_Refresh_Handler,
		},
		{
			MethodName: "Renew",
			Handler:    _Users_Renew_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "users.proto",
}
