#! /usr/bin/python
# microservices dumb example
# Copyright (C) 2020 Bruno Mondelo Giaramita
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import grpc
import importlib
import sys

# Ask for service host and port
users_host = input('Users host: ')
users_port = input('Users port: ')
users_str = '%s:%s' % (users_host, users_port)

# Protocol buffer directory
PROTO_DIR = 'protobuf-python'

# PB2 file name
USERS_PB2_FILE = 'users_pb2'
TOKEN_PB2_FILE = 'token_pb2'

# PB2 GRPC file name
USERS_PB2_GRPC_FILE = 'users_pb2_grpc'
TOKEN_PB2_GRPC_FILE = 'token_pb2_grpc'

# Import protocol buffer
old_path = sys.path
sys.path.append(PROTO_DIR)
USERS_PB2 = importlib.import_module('%s.%s' % (PROTO_DIR, USERS_PB2_FILE))
USERS_PB2_GRPC = importlib.import_module('%s.%s' % (PROTO_DIR,
                                                    USERS_PB2_GRPC_FILE))
TOKEN_PB2 = importlib.import_module('%s.%s' % (PROTO_DIR, TOKEN_PB2_FILE))
TOKEN_PB2_GRPC = importlib.import_module('%s.%s' % (PROTO_DIR,
                                                    TOKEN_PB2_GRPC_FILE))
sys.path = old_path


def users_register(email, username, password):
    """Executes the grpc Users.Register

    Parameters
    ----------
    email : str
        User email
    username : str
        User username
    password : str
        User password

    Returns
    -------
    USERS_PB2.UserJWT
    """
    with grpc.insecure_channel(users_str) as channel:
        stub = USERS_PB2_GRPC.UsersStub(channel)
        return stub.Register(
            USERS_PB2.User(
                email=email,
                username=username,
                password=password,
            )
        )


def users_login(email, username, password):
    """Executes the grpc Users.Login

    Parameters
    ----------
    email : str
        User email
    username : str
        User username
    password : str
        User password

    Returns
    -------
    USERS_PB2.UserJWT
    """
    with grpc.insecure_channel(users_str) as channel:
        stub = USERS_PB2_GRPC.UsersStub(channel)
        return stub.Login(
            USERS_PB2.User(
                email=email,
                username=username,
                password=password,
            )
        )


def users_logout(token):
    """Executes the grpc Users.Logout

    Parameters
    ----------
    token : str

    Returns
    -------
    empty
    """
    with grpc.insecure_channel(users_str) as channel:
        stub = USERS_PB2_GRPC.UsersStub(channel)
        return stub.Logout(
            TOKEN_PB2.JWT(
                token=token,
            )
        )


def users_authenticate(token):
    """Executes the grpc Users.Authenticate

    Parameters
    ----------
    token : str
        User token

    Returns
    -------
    USERS_PB2.User
    """
    with grpc.insecure_channel(users_str) as channel:
        stub = USERS_PB2_GRPC.UsersStub(channel)
        return stub.Authenticate(
            TOKEN_PB2.JWT(
                token=token,
            )
        )


def users_refresh(token):
    """Executes the grpc Users.Refresh

    Parameters
    ----------
    token : str
        User token

    Returns
    -------
    TOKEN_PB2.JWT
    """
    with grpc.insecure_channel(users_str) as channel:
        stub = USERS_PB2_GRPC.UsersStub(channel)
        return stub.Refresh(
            TOKEN_PB2.JWT(
                token=token,
            )
        )


def users_renew(token):
    """Executes the grpc Users.Renew

    Parameters
    ----------
    token : str
        User token

    Returns
    -------
    TOKEN_PB2.JWT
    """
    with grpc.insecure_channel(users_str) as channel:
        stub = USERS_PB2_GRPC.UsersStub(channel)
        return stub.Renew(
            TOKEN_PB2.JWT(
                token=token,
            )
        )
